# 4 - Largest Palindrome Product
# https://projecteuler.net/problem=4
# Answer: 906609

largest = -1

def is_palindrome(_s):
    '''Compares the first and last characters in the string and moves to the center, 
    ex: [X,#,#,#,#,X] --> [#,X,#,#,X,#] --> [#,#,X,X,#,#]'''

    # If odd return false
    if (len(_s) % 2 == 1):
        return False
    
    for i in range(int(len(_s)/2)):
        if (_s[i] != _s[-(i + 1)]):
            return False
        
    return True

# Loops through two dimensions of 3 digit numbers one at a time and calcs the product and checks if palindrome for each
for i in range(100, 1000): # 3 Digit numbers
    for j in range(100, 1000):
        product = i * j
        if is_palindrome(str(product)):
            if product > largest:
                print(f"Valid: {product}")
                largest = product

print(f"Largest: {largest}")