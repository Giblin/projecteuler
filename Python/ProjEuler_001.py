# 1 - Multiples of 3 or 5
# https://projecteuler.net/problem=1
# Answer: 233168

RANGE_LIMIT = 1000 # Q: 1000

total = 0
for i in range(RANGE_LIMIT):
    if i % 3 == 0 or i % 5 == 0:
        total += i
print(total)