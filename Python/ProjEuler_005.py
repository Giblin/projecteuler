# 5 - Smallest Multiple
# https://projecteuler.net/problem=5
# Answer: 232792560

NUM_LIMIT = 21 # Q: 21

num = 0

def check_range_divisable(_n):
    '''Checks if every element in a range is evenly divisible by a number(_n)'''
    for i in range(1, NUM_LIMIT):
        if (_n % i != 0):
            return False

    return True

while True:
    num += 1

    if check_range_divisable(num):
        print(num)
        break