# 6 - Sum Square Difference
# https://projecteuler.net/problem=6
# Answer: 25164150

NUM_LIMIT = 100 # Q: 100

def square_of_sum(_n):
    '''Adds each number in a range and squares the result'''
    total = 0

    for i in range(1, _n + 1):
        total += i

    return pow(total, 2)

def sum_of_squares(_n):
    '''Adds the squared values of each number in a range'''
    total = 0
    for i in range(1, _n + 1):
        total += pow(i, 2)
    
    return total

square_of_sum_ans = square_of_sum(NUM_LIMIT)
sum_of_squares_ans = sum_of_squares(NUM_LIMIT)

print(f"{square_of_sum_ans} - {sum_of_squares_ans} = {square_of_sum_ans - sum_of_squares_ans}")