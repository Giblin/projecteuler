# 7 - 10001st Prime
# https://projecteuler.net/problem=7
# Answer: 104743

import math

NUM_OF_PRIME = 10001 #Q: 10001

def is_prime(_n):
	# Needed to check if prime but not used in this implementation in the name of SPEED
    #if (n == 0) or (n == 1):
    #    return False
    
    for i in range(2, int(math.sqrt(_n)) + 1):
        if _n % i == 0:
            return False
    
    return True

# Loops through as many nums as needed until the required number of primes is hit,
# ends loop and prints last num
num = 1 # Starts at 1 (actually 2 because it increments in the loop before checking) to skip 0 & 1
prime_count = 0
while prime_count < NUM_OF_PRIME:
    num += 1
    if is_prime(num):
        prime_count += 1

print(f"Prime #{NUM_OF_PRIME} is: {num}")
