# 10 - Summation of Primes
# https://projecteuler.net/problem=10
# Answer: 142913828922

import math

NUM_LIMIT = 2000000 # Q: 2000000

def is_prime(_n):
    # Needed to check if prime but not used in this implementation in the name of SPEED
    # if (n == 1 or n == 0):
    #    return False

    for i in range(2, int(math.sqrt(_n)) + 1):
        if(_n % i == 0):
            return False

    return True

total = 0
for n in range(2, NUM_LIMIT):
    if is_prime(n):
        total += n
    
print(total)