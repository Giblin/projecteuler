# 9 - Special Pythagorean Triplet
# https://projecteuler.net/problem=9
# Answer: 31875000

import math

TARG_NUM = 1000 # Q: 1000

def is_pythag_triplet(a, b, c):
    if ((pow(a, 2) + pow(b, 2)) == pow(c, 2)):
        return True
    return False

b = 2 #a < b & a >= 1 so b starts at 2
# Loops through every valid pythagorean triplet.
# b increments by 1 and each time every prossible a from 1 to b - 1 is checked.
# c is calculated and if c is a natural number it is a valid triplet.
# If valid then check sum against target sum to find answer.
while True:
    for a in range(1, b - 1):
        c = math.sqrt(pow(a, 2) + pow(b, 2))
        if not c.is_integer():
            continue
        if a + b + c == TARG_NUM:
            print(f"a:{a} b:{b} c:{c:.0f} product:{(a*b*c):.0f}")
            quit()
    b += 1