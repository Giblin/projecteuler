# 12 - Highly Divisible Triangular Number
# https://projecteuler.net/problem=12
# Answer: 76576500

import math

DIVISORS = 500 # Q: 500

def get_triangle_num(_n : int):
    '''Adds all numbers up to _n and returns the sum'''
    total = 0
    for n in range(1, _n + 1):
        total += n
    return total
    
def count_divisors(_n : int):
    '''Counts the number of divisors in a number(_n)
    by checking if each number up to the sqrt(_n) + 1'''
    count = 0
    for n in range(1, int(math.sqrt(_n)) + 1):
        if (_n % n == 0):
            if (_n / n == n): # If same divisor only count once
                count += 1
            else: # Otherwise count both
                count += 2
    return count

num = 1
while True:
    num += 1
    
    triangle_num = get_triangle_num(num)
    divisors = count_divisors(triangle_num)
    
    if (divisors > DIVISORS):
        print(f"Triangle Number #{num} = {triangle_num} : Divisors:{divisors}")
        break