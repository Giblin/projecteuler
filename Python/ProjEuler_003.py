# 3 - Largest Prime Factor
# https://projecteuler.net/problem=1
# Answer: 6857

import math

NUM_TO_FACTOR = 600851475143 # Q: 600851475143

def is_prime(_n):
    # Needed to check if prime but not used in this implementation in the name of SPEED
    # if (n == 1 or n == 0):
    #    return False

    for i in range(2, int(math.sqrt(_n)) + 1):
        if(_n % i == 0):
            return False

    return True

# Start at 2 and go to the square root of the num which is the highest possible prime factor
for n in range(2, int(math.sqrt(NUM_TO_FACTOR))):
    if is_prime(n) and NUM_TO_FACTOR % n == 0: # If prime & if factor of the num
        print(f"Valid: {n}")
