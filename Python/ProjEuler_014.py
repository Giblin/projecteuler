# 14 - Longest Collatz Sequence
# https://projecteuler.net/problem=14
# Answer: 837799

LIMIT_NUM = 1000000 # Q: 1000000

def get_collatz_sequence_length(_n):
    length = 1
    
    while _n > 1:
        length += 1
        
        if _n % 2 == 0: # Even
            _n = _n / 2
        else: #Odd
            _n = 3 * _n + 1
    
    return length

longest = num = -1
for n in range(1, LIMIT_NUM):
    if (new_longest := get_collatz_sequence_length(n)) > longest:
        longest = new_longest
        num = n

print(f"{num} is the longest with {longest} numbers")