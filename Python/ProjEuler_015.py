# 15 - Longest Collatz Sequence
# https://projecteuler.net/problem=15
# Answer: 137846528820

GRID_SIZE = 20 # Q: 20

def factorial(_n : int):
    product = 1
    while _n > 1:
        product *= _n
        _n -= 1
    return product

def solve_binomial_coefficient(_n : int, _k : int):
    return int(factorial(_n) / (factorial(_k) * (factorial(_n - _k))))

print(solve_binomial_coefficient(GRID_SIZE * 2, GRID_SIZE))