# 2 - Even Fibonacci Numbers
# https://projecteuler.net/problem=2
# Answer: 4613732

NUM_LIMIT = 4000000 # Q: 4000000

num_1 = 1
num_2 = 2
total = num_2

# Calculates the next fib number each loop and checks if it exceeds the limit
# if not then assign the new val to the num variables and check if even
while True:
    new_val = num_1 + num_2

    if (new_val > NUM_LIMIT):
        break

    num_1 = num_2
    num_2 = new_val
    if (num_2 % 2 == 0): # Even only
        total += num_2

print(total)